package org.jproggy.pagesally.generator.format

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import org.jproggy.snippetory.SnippetoryException
import org.jproggy.snippetory.Syntaxes.XML_ALIKE
import org.jproggy.snippetory.TemplateContext
import org.jproggy.snippetory.util.NoDataException

class PageLinkTest : FunSpec({
  val ctx = TemplateContext().syntax(XML_ALIKE).uriResolver{ x, y ->
    if (x == "content/x/y/index.page") "sdfdsf" else throw NoDataException(x)
  }
  test("simple") {
    ctx.parse("<t: page='/x/y/'>xx</t:>").toString() shouldBe "<a href=\"/x/y/\">xx</a>"
  }
  test("classAttr") {
    ctx.parse("<t: page='/x/y/' page.class=\"test\">xx</t:>").toString() shouldBe "<a href=\"/x/y/\" class=\"test\">xx</a>"
  }
  test("xxAttr") {
    ctx.parse("<t: page='/x/y/' page.xx=\"test\">xx</t:>").toString() shouldBe "<a href=\"/x/y/\" xx=\"test\">xx</a>"
  }
  test("templateAttr") {
    val tpl = ctx.parse("<t: page='/x/y/' page.class=\"{v:test}\">xx</t:>")
    tpl.names() shouldBe setOf("test")
    tpl.set("test", "yy").toString() shouldBe "<a href=\"/x/y/\" class=\"yy\">xx</a>"
  }
  test("not found") {
    shouldThrow<SnippetoryException> {
      ctx.parse("<t: page='/x/y/z'>xx</t:>")
    }
  }
})
