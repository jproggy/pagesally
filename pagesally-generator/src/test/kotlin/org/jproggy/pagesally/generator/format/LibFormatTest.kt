package org.jproggy.pagesally.generator.format

import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import org.jproggy.snippetory.TemplateContext
import org.jproggy.snippetory.util.NoDataException

class LibFormatTest : FunSpec({
 val ctx = TemplateContext().uriResolver { x, y ->
  if (x == "content/frame.html") """
   <t:library><t:test>Well done<t:>, {v:content}</t:>!</t:test></t:library>
  """.trimIndent() else throw NoDataException(x)
 }
  test("formatVoid") {
   ctx.parse("{v: lib='test'}").toString() shouldBe "Well done!"
   ctx.parse("<t: lib='test'>xx</t:>").toString() shouldBe "Well done, xx!"
   ctx.parse("<t: lib='test'></t:>").toString() shouldBe "Well done!"
  }
})
