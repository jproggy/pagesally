package org.jproggy.pagesally.generator.format

import org.jproggy.pagesally.generator.format.PageLink.Format
import org.jproggy.snippetory.Encodings
import org.jproggy.snippetory.SnippetoryException
import org.jproggy.snippetory.Syntaxes
import org.jproggy.snippetory.Template
import org.jproggy.snippetory.TemplateContext
import org.jproggy.snippetory.spi.DynamicAttributes
import org.jproggy.snippetory.spi.FormatConfiguration
import org.jproggy.snippetory.spi.VoidFormat
import org.jproggy.snippetory.util.StateContainer
import org.jproggy.snippetory.util.TemplateNode


class PageLink(val target: String, val ctx: TemplateContext) :  StateContainer<Format>(KeyResolver.up(0)),
    FormatConfiguration, DynamicAttributes  {
    private val attribs: MutableMap<String, Template> = HashMap()
    private val names: MutableSet<String> = HashSet()

    override fun controlsRegion() = true

    override fun getFormat(node: TemplateNode): Format = get(node)

    override fun createValue(node: TemplateNode): Format = Format(this)

    override fun setAttribute(name: String, value: String) {
        attribs[name] = ctx.parse(value).also { names.addAll(it.names()) }
    }

    init {
        if (!target.startsWith("/")) {
            throw SnippetoryException("Relative page found: $target")
        }
        var uri = target.split("#".toRegex()).toTypedArray()[0]
        if (uri.endsWith("/")) uri += "index.page"
        if (uri.endsWith(".html")) uri = uri.substring(0, uri.length - 4) + "page"
        ctx.uriResolver.resolve("content$uri", ctx) ?: throw SnippetoryException("$target not found")
    }


    class Format(private val config: PageLink) : VoidFormat {
        private val attribs: MutableMap<String, Template> = HashMap()
        private val page: Template = Syntaxes.XML_ALIKE.context().encoding(Encodings.html)
            .parse("""<a href="{v:target}"<t:attrib> {v:name}="{v:value}"</t:>>{v:content}</a>""")

        override fun formatVoid(node: TemplateNode): Any {
            val t = page.get().set("target", config.target).set("content", node.region())
            attribs.forEach { (k: String, v: Template) -> t["attrib"].set("name", k).set("value", v).render() }
            return t
        }

        override fun append(name: String?, value: Any?) {
            attribs.forEach { k, v ->
                v.append(name, value)
            }
        }

        override fun set(name: String?, value: Any?) {
            attribs.forEach { k, v ->
                v.set(name, value)
            }
        }

        override fun names(): MutableSet<String> = config.names

        override fun clear(node: TemplateNode) {
            attribs.forEach { _, v -> v.clear() }
        }

        init {
            config.attribs.forEach { k, v -> attribs[k] = v.get() }
        }
    }
}