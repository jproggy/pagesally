package org.jproggy.pagesally.generator

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import org.jetbrains.kotlin.konan.file.File
import java.nio.file.FileSystems
import java.nio.file.FileVisitResult
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.SimpleFileVisitor
import java.nio.file.StandardWatchEventKinds.*
import java.nio.file.WatchEvent.Kind
import java.nio.file.WatchKey
import java.nio.file.attribute.BasicFileAttributes

data class FileEvent(
  val file: Path,
  val op: Kind<out Any>
)

fun File.asPath(): Path = Path.of(path)

@ExperimentalCoroutinesApi
fun File.watch(scope: CoroutineScope = GlobalScope, vararg events: Kind<Path> = arrayOf(ENTRY_CREATE, ENTRY_MODIFY, ENTRY_DELETE)) : Channel<FileEvent> {
  val watchService = FileSystems.getDefault().newWatchService()
  val registeredKeys = HashMap<WatchKey, Path>()
  val channel: Channel<FileEvent> = Channel()
  val path = this.asPath()
  Files.walkFileTree(path, object : SimpleFileVisitor<Path>() {
    override fun preVisitDirectory(subPath: Path, attrs: BasicFileAttributes): FileVisitResult {
      registeredKeys += subPath.register(watchService, events) to path.relativize(subPath)
      return FileVisitResult.CONTINUE
    }
  })
  scope.launch(Dispatchers.IO) {
    while (!channel.isClosedForSend) {
      val monitorKey = watchService.take()
      monitorKey.pollEvents().forEach { event ->
        registeredKeys[monitorKey]?.let {
          channel.send(FileEvent(it.resolve(event.context() as Path), event.kind()))
        }
      }

      if (!monitorKey.reset()) {
        registeredKeys.remove(monitorKey)
        monitorKey.cancel()
        if (registeredKeys.isEmpty()) {
          channel.close()
          break
        }
      }
    }
  }
  channel.invokeOnClose {
    registeredKeys.apply {
      forEach { k, _ -> k.cancel() }
      clear()
    }
  }
  return channel
}

