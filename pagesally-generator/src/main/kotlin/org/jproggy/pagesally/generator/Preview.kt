package org.jproggy.pagesally.generator

import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpServer
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.jetbrains.kotlin.konan.file.File
import java.net.InetSocketAddress
import java.nio.file.Files
import java.util.*
import java.util.concurrent.Executors

const val FILE_CHANGE = "/_next_file_change/"
const val RELOADER =
      """
      <script>
          const req = new XMLHttpRequest();
          req.onload = (e) => window.location.reload();
          req.open('GET', 'http://' + window.location.host + '$FILE_CHANGE');
          req.send();
      </script>
      """
class Preview
  (private val gen: Generator, private val store: Map<String, PagesStore.Page>,
              private val resources: Map<String, File>) {
  private val log = KotlinLogging.logger {}
  private val jobs: MutableList<Job> = ArrayList()
  @OptIn(ExperimentalCoroutinesApi::class)
  val events = File(gen.root.absolutePath).watch()
  fun run() {
    val server = HttpServer.create(InetSocketAddress("localhost", 8081), 0);
    server.createContext("/") { req ->
      req.responseHeaders.set("Server", "pagesally-shower")
      try {
        store[req.requestURI.path]?.let {
          req.responseHeaders.set("Content-Type", "text/html")
          req.write(200, gen.generatePage(it).toString() + RELOADER)
          cancelJobs()
        } ?: resources[req.requestURI.path]?.let { data ->
          Files.probeContentType(data.asPath())
          req.responseHeaders.set("Content-Type", Files.probeContentType(data.asPath()))
          req.write(200, data.readBytes())
        } ?: if (req.requestURI.path == FILE_CHANGE) {
          log.info("waiting")
          var job: Job
          runBlocking {
            job = launch {
              events.receive().also { log.info("received ${it.file}") }
            }.also { jobs.add(it) }
          }
          // if the job was cancelled, providing a response doesn't work.
          // Maybe because different requests/same connection
          if (!(job.isCancelled)) {
            req.responseHeaders.set("Content-Type", "text/plain")
            req.write(200, "reload")
          } else {
            // do nothing
          }
        } else {
          req.write(404, "Not found")
        }
      } catch (e: Exception) {
        log.error("${req.requestURI} failed", e)
      }
    }.
    server.executor = Executors.newFixedThreadPool(10);
    server.start();
  }

  private fun cancelJobs() {
    val i = jobs.iterator()
    while (i.hasNext()) {
      val job = i.next()
      job.cancel()
      log.info("Job canceled")
      i.remove()
    }
  }

  private fun HttpExchange.write(rCode: Int, data: String) {
    write(rCode, data.toByteArray())
  }

  private fun HttpExchange.write(rCode: Int, bytes: ByteArray) {
    this.sendResponseHeaders(200, bytes.size.toLong())
    this.responseBody.use { it.write(bytes) }
    log.info("${this.requestURI} $rCode ${bytes.size}")
  }

  companion object {
    @JvmStatic
    fun main(args: Array<String>) {
      val baseDir = File(args[0])
      val resourcesDir = File(baseDir, "resources")
      val pages = PagesStore.at(baseDir)
      val gen = Generator(pages)
      val store = TreeMap<String, PagesStore.Page>(String.CASE_INSENSITIVE_ORDER).addPages(pages.content())
      val resources = TreeMap<String, File>().list(resourcesDir, "")
      Preview(gen, store, resources).run()
    }
  }
}

private fun MutableMap<String, PagesStore.Page>.addPages(dir: PagesStore.Folder): MutableMap<String, PagesStore.Page> {
  for (page in dir.pages()) {
    this[page.url()] = page
  }
  dir.children().forEach { addPages(it) }
  return this
}

private fun MutableMap<String, File>.list(folder: File, name: String): Map<String, File> {
  for (entry in folder.listFiles) {
    if (entry.isDirectory)  {
      list(entry, name + "/" + entry.name)
    } else {
      this[name + "/" + entry.name] = entry
    }
  }
  return this
}
