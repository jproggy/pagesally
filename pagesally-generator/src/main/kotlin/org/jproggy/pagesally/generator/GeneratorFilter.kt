package org.jproggy.pagesally.generator

import jakarta.servlet.Filter
import jakarta.servlet.FilterChain
import jakarta.servlet.FilterConfig
import jakarta.servlet.ServletException
import jakarta.servlet.ServletRequest
import jakarta.servlet.ServletResponse
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch
import org.jetbrains.kotlin.konan.file.File
import org.jproggy.pagesally.generator.PagesStore.Folder
import org.jproggy.pagesally.generator.PagesStore.Page
import java.io.IOException
import java.nio.file.StandardWatchEventKinds.ENTRY_CREATE
import java.util.*

@ExperimentalCoroutinesApi
class GeneratorFilter : Filter {
    private var gen: Generator? = null
    private var store: Map<String, Page>? = null
    private var resources: Map<String, File>? = null

    @Throws(IOException::class, ServletException::class)
    override fun doFilter(req: ServletRequest, resp: ServletResponse, chain: FilterChain) {
        filter(req as HttpServletRequest, resp as HttpServletResponse, chain)
    }

    @Throws(ServletException::class, IOException::class)
    private fun filter(req: HttpServletRequest, resp: HttpServletResponse, chain: FilterChain) {
        store!![pageUri(req)]?.let {
            gen!!.generatePage(it).render(resp.writer)
        } ?: resources!![req.requestURI ?: ""]?.let { data ->
            resp.outputStream.write(data.readBytes())
            req.servletContext.getMimeType(req.requestURI)?.let { type ->
                resp.contentType = type
            }
        } ?: chain.doFilter(req, resp)
    }

    private fun pageUri(req: HttpServletRequest): String = req.requestURI ?: ""

    override fun init(config: FilterConfig) {
        val pages = PagesStore.at(baseDir(config))
        gen = Generator(pages)
        store = TreeMap<String, Page>(String.CASE_INSENSITIVE_ORDER).addPages(pages.content())
        resources = resources(config)
        val scope = CoroutineScope(Dispatchers.Default)
        scope.launch {
            pages.content().location().watch(scope, ENTRY_CREATE).consumeEach {
                store = TreeMap<String, Page>(String.CASE_INSENSITIVE_ORDER).addPages(pages.content())
                resources = resources(config)
            }
        }
    }

    private fun baseDir(config: FilterConfig): File {
        val dir: String? = config.getInitParameter("baseDir")
        return dir?.let { File(dir) } ?: throw IllegalStateException("baseDir not set")
    }

    private fun resources(config: FilterConfig): Map<String, File> {
        val dir = File(baseDir(config), "resources")
        return TreeMap<String, File>().list(dir, "")
    }

    override fun destroy() {
        // required
    }
}

private fun MutableMap<String, Page>.addPages(dir: Folder): MutableMap<String, Page> {
    for (page in dir.pages()) {
        this[page.url()] = page
    }
    dir.children().forEach { addPages(it) }
    return this
}

private fun MutableMap<String, File>.list(folder: File, name: String): Map<String, File> {
    for (entry in folder.listFiles) {
        if (entry.isDirectory)  {
            list(entry, name + "/" + entry.name)
        } else {
            this[name + "/" + entry.name] = entry
        }
    }
    return this
}


