package org.jproggy.pagesally.generator.format

import org.jproggy.snippetory.spi.Configurer
import org.jproggy.snippetory.spi.Format

class Initializer : Configurer {

    companion object {
        init {
            Format.register("lib", ::LibFormat)
            Format.register("page", ::PageLink)
        }
    }
}