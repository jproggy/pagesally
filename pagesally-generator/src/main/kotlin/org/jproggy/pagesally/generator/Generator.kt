package org.jproggy.pagesally.generator

import mu.KotlinLogging
import org.jetbrains.kotlin.konan.file.File
import org.jproggy.pagesally.generator.PagesStore.Page
import org.jproggy.snippetory.Template
import org.jproggy.snippetory.spi.Metadata
import java.io.IOException
import java.util.*

private val log = KotlinLogging.logger {}

class Generator(private val pages: PagesStore) {
    val root: File = pages.root.location()

    companion object {
        private const val EXCLUDE = "exclude"
        private const val INCLUDE = "include"
        @Throws(IOException::class)
        @JvmStatic
        fun main(args: Array<String>) {
            if (args.size < 2 || args.size > 3) {
                println("Usage: <from> <to> [<locale>]")
                return
            }
            val to = File(args[1])
            val pages = PagesStore.at(File(args[0]))
            if (args.size == 3) pages.locale(Locale.of(args[2]))
            Metadata.registerAnnotation(EXCLUDE)
            Metadata.registerAnnotation(INCLUDE)
            Generator(pages).generate(to)
        }
    }

    fun generate(target: File): Unit {
        pages.collectors().navigate{ collector ->
            val tpl = collector.load()
            tpl.regionNames().forEach { regName ->
                val metadata = tpl[regName].metadata()
                val exclude = metadata.annotation(EXCLUDE).orElse("").toRegex()
                val include = metadata.annotation(INCLUDE).orElse(".*").toRegex()
                pages.content().navigate { page ->
                    if (include.containsMatchIn(page.path()) && !exclude.containsMatchIn(page.path())) {
                        renderPage(tpl[regName], page)
                    }
                }
            }
            tpl.render(collector.to(target))
        }
        if (pages.content().location().exists) {
            log.info("Generating ${pages.content().location().absolutePath} into " + target.absolutePath)
            pages.content().navigate { page ->
                generatePage(page).render(page.to(target))
            }
        } else {
            log.warn(pages.content().location().absolutePath + " doesn't exist")
        }
    }

    private fun renderPage(target: Template, page: Page) {
        target
            .set("targetUrl", page.url())
            .set("sourcePath", page.path())
        val pageTpl = page.load()
        pageTpl.regionNames().forEach {
            target.set(it, pageTpl[it])
        }
        target.render()
    }

    fun generatePage(pageSource: Page): Template {
        val frame = pages.frame()
        val menu: Template = pageSource.menu()
        log.info {"Generating page " + pageSource.path() }
        val page = pageSource.load()
        handleRegions(menu, frame)
        handleRegions(page, frame)
        handleRegions(page, menu)
        menu.set("content", page)
        return frame.set("menu", menu)
    }

    private fun handleRegions(spec: Template, target: Template) {
        spec.regionNames().forEach {
            target.set(it, spec[it])
        }
    }
}
