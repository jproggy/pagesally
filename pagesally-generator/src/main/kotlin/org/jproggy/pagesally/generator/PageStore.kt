package org.jproggy.pagesally.generator

import org.jetbrains.kotlin.konan.file.File
import org.jproggy.snippetory.Encodings.html
import org.jproggy.snippetory.SnippetoryException
import org.jproggy.snippetory.Syntaxes.FLUYT_XA
import org.jproggy.snippetory.Template
import org.jproggy.snippetory.TemplateContext
import org.jproggy.snippetory.UriResolver
import org.jproggy.snippetory.spi.Configurer
import java.io.IOException
import java.io.Writer
import java.util.*


class PagesStore private constructor(private val storeLocation: File) : Configurer {
    val root = RootFolder()
    private val ctx: TemplateContext = TemplateContext()
        .uriResolver(UriResolver.directories(storeLocation.absolutePath))
        .encoding(html)
        .syntax(FLUYT_XA)

    fun locale(l: Locale?): PagesStore {
        ctx.locale(l)
        return this
    }

    private fun root(): RootFolder = root

    fun content(): Folder = root().child("content")
    fun collectors(): Folder = root().child("collectors")

    fun frame(): Template = ctx.getTemplate("content/frame.html")

    abstract inner class Folder {
        abstract fun path() : String
        abstract fun url(): String
        abstract fun menu(): Template

        fun location(): File = storeLocation.child(path())

        private fun locationsBy(filter: (File) -> Boolean): List<File> {
            if (!location().exists) return emptyList()
            return location().listFiles.filter(filter)
        }

        fun children(): List<Folder> {
            return locationsBy {
                it.isDirectory && !it.name.startsWith(".")
            }.map { SubFolder(this, it.name )}
        }

        fun pages(): List<Page> {
            return locationsBy {
                it.isFile && it.name.endsWith(PAGE_SUFFIX)
            }.map { Page(this, it.name) }
        }

        fun navigate(handler: (page: Page) -> Unit) {
            pages().forEach {
                handler(it)
            }
            children().forEach {
                it.navigate(handler)
            }
        }
    }

    inner class RootFolder : Folder() {
        override fun path() = ""
        override fun menu(): Template {
            val navFile = location().child(MENU)
            return if (navFile.exists)
                ctx.getTemplate(path() + MENU)
            else {
                ctx.parse("{v:content}")
            }
        }

        fun child(name: String) = SubFolder(this, name)
        override fun url(): String = ""

    }

    inner class SubFolder(private val parent: Folder, private val name: String) : Folder() {
        private val path: String = if (parent.path().isEmpty())
            name + '/'
        else
            parent.path() + name + '/'

        override fun path() = path
        override fun menu(): Template {
            val navFile = location().child(MENU)
            return if (navFile.exists)
                ctx.getTemplate(path() + MENU)
            else {
                parent.menu()
            }
        }
        override fun url(): String {
            if (parent is RootFolder) {
                return "/"
            }
            return parent.url() + name + '/'
        }
    }

    inner class Page(val parent: Folder, name: String) {
        private val name = name.substring(0, name.length - PAGE_SUFFIX.length)

        fun path(): String = parent.path() + name
        fun load(): Template {
            try {
                return ctx.getTemplate(path() + PAGE_SUFFIX)
            } catch (e: SnippetoryException) {
                throw SnippetoryException("Parsing of ${url()} failed.", e)
            }
        }
        fun url(): String {
            if (name == "index") {
                return parent.url()
            }
            return parent.url() + name + ".html"
        }

        fun menu(): Template = parent.menu()

        @Throws(IOException::class)
        fun to(targetDir: File): Writer {
            val targetFile = targetDir.child(parent.url().substring(1) + name + ".html")
            targetFile.parentFile.mkdirs()
            return targetFile.printWriter()
        }
    }

    companion object {
        private const val PAGE_SUFFIX = ".page"
        private const val MENU = "menu.html"
        fun at(root: File) = PagesStore(root)
    }
}
