package org.jproggy.pagesally.generator.format

import org.jproggy.snippetory.SnippetoryException
import org.jproggy.snippetory.Template
import org.jproggy.snippetory.TemplateContext
import org.jproggy.snippetory.spi.DynamicAttributes
import org.jproggy.snippetory.spi.VoidFormat
import org.jproggy.snippetory.util.CharDataSupport
import org.jproggy.snippetory.util.SimpleFormat
import org.jproggy.snippetory.util.TemplateNode

class LibFormat(private val name: String, ctx: TemplateContext) : SimpleFormat(), DynamicAttributes, VoidFormat {
    private val lib: Template
    private val attribs: MutableMap<String, String> = HashMap()
    override fun format(location: TemplateNode, value: Any?): Any {
        val tpl = lib[name]
        attribs.forEach { (name: String?, value: String?) -> tpl[name] = value }
        return tpl.set("content", value)
    }

    override fun formatVoid(node: TemplateNode): Any {
        return format(node, if (node.region().toString().isEmpty()) Template.NONE else node.region())
    }

    override fun controlsRegion(): Boolean {
        return true
    }

    override fun supports(value: Any): Boolean {
        return CharDataSupport.isCharData(value)
    }

    override fun setAttribute(name: String, value: String) {
        attribs[name] = value
    }

    init {
        lib = ctx.getTemplate("content/frame.html")["library"]
        if (!lib.regionNames().contains(name)) {
            throw SnippetoryException("$name not found in library")
        }
    }
}