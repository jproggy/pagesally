val versions: Map<String, String> by rootProject.extra

dependencies {
    compileOnly("jakarta.servlet", "jakarta.servlet-api", versions["web-api"])

    implementation(kotlin("util-io"))

    implementation("org.jproggy", "snippetory-core", versions["snippetory"])
    implementation("io.github.microutils", "kotlin-logging", versions["kotlin-logging"])
    implementation("org.slf4j", "slf4j-api", versions["slf4j"])
    implementation("org.apache.logging.log4j","log4j-slf4j-impl", versions["log4j"]).exclude(
        group = "org.slf4j"
    )
}

publishing.publications.named("pagesally", MavenPublication::class) {
    pom {
        description.set("Generator for static web sites")
        url.set("https://www.jproggy.org/pagesally/generator/")
        licenses {
            license {
                name.set("The Apache License, Version 2.0")
                url.set("http://www.apache.org/licenses/LICENSE-2.0")
                distribution.set("repo")
            }
        }
    }
}
