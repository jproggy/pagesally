package org.jproggy.pagesally.xml

import org.w3c.dom.Element
import org.w3c.dom.Node
import org.w3c.dom.NodeList
import org.xml.sax.InputSource
import javax.xml.XMLConstants
import javax.xml.parsers.DocumentBuilderFactory

typealias DomDocument = org.w3c.dom.Document
object Xml {
    fun parse(content: InputSource?): Document {
        val factory = DocumentBuilderFactory.newInstance()
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "")
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "")
        factory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false)
        factory.setFeature("http://xml.org/sax/features/validation", false)
        val builder = factory.newDocumentBuilder()
        return Document(builder.parse(content))
    }

    fun parseResource(name: String): Document {
        val data = this::class.java.getResourceAsStream(name)
        return parse(InputSource(data))
    }

    class Document(private val doc: DomDocument) {
        val element: Tag get() = Tag(doc.documentElement)
    }

    class Tag(private val tag: Element) {
        val children: List<Tag> by lazy { getChildren(tag.childNodes) }
        val name: String  get() = tag.tagName?:""
        val content: String get() = tag.textContent?:""
        operator fun get(name: String): String? = if (tag.hasAttribute(name)) tag.getAttribute(name) else null
        fun child(name: String) = children.byName(name).first()
        private fun getChildren(nodes: NodeList): List<Tag> {
            val children = ArrayList<Tag>()
            for (i in 0 until nodes.length) {
                if (nodes.item(i).nodeType == Node.ELEMENT_NODE) {
                    children.add(Tag(nodes.item(i) as Element))
                }
            }
            return children
        }
    }

    fun List<Tag>.byName(name: String) = filter { it.name == name }
}


