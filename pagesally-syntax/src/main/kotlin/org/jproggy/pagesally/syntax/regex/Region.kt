package org.jproggy.pagesally.syntax.regex

import org.jproggy.pagesally.syntax.regex.Content.LINE
import org.jproggy.pagesally.syntax.regex.Content.LINE_END

interface Region {
    fun build(): Regex
    interface Multilinable : Region {
        fun multiline(): Regex
    }

    interface Closable : Region {
        fun to(end: String): Multilinable
        fun onLineStart(): Regex
    }

    companion object {
        fun from(start: String): Closable {
            return object : Closable {
                override fun to(end: String): Multilinable {
                    return object : Multilinable {
                        override fun multiline(): Regex {
                            return (Regex.escape(start) + "[\\s\\S]*?" + Regex.escape(end))
                                .toRegex(RegexOption.MULTILINE)
                        }

                        override fun build(): Regex {
                            return (Regex.escape(start) + ".*?" + Regex.escape(end)).toRegex()
                        }
                    }
                }

                override fun onLineStart(): Regex {
                    return ("^" + Regex.escape(start) + LINE + LINE_END).toRegex()
                }

                override fun build(): Regex {
                    return (Regex.escape(start) + LINE + LINE_END).toRegex()
                }
            }
        }
    }
}