package org.jproggy.pagesally.syntax

data class TextSegment(private val data: CharSequence, val pos: IntRange = data.indices) : CharSequence {
    override val length: Int
        get() = (pos.last - pos.first) + 1

    override fun get(index: Int) = data[pos.first + index]

    override fun subSequence(startIndex: Int, endIndex: Int): CharSequence {
        return subSegment(startIndex, endIndex - 1)
    }

    fun subSegment(range: IntRange): TextSegment {
        return subSegment(range.first, range.last)
    }

    fun subSegment(startIndex: Int = 0, endIndex: Int = length - 1): TextSegment {
        assert((pos.first + endIndex) <= pos.last)
        assert(startIndex >= 0)
        assert(endIndex >= (startIndex - 1))
        return TextSegment(data, (pos.first + startIndex)..(pos.first + endIndex))
    }

    override fun toString() = data.substring(pos)

    init {
        assert(pos.first >= 0)
        assert(pos.last >= (pos.first - 1))
        assert(pos.last < data.length)
    }

    fun split(regex: Regex): List<TextSegment> {
        val segments = ArrayList<TextSegment>()
        var pos = 0
        for (x in regex.findAll(this)) {
            if (pos < x.range.first) {
                segments.add(subSegment(pos, x.range.first - 1))
            }
            segments.add(subSegment(x.range.first, x.range.last))
            pos = x.range.last + 1
        }

        if (pos < length) {
            segments.add(subSegment(pos, length - 1))
        }
        return segments
    }

    fun <T : Any> transform(regex: Regex, nonMatch: (segment: TextSegment) -> T) =
        MatchSequenceBuilder(this, regex, nonMatch)
}

class MatchSequenceBuilder<T : Any>(
    private val data: TextSegment,
    private val regex: Regex,
    private val converter: (data: TextSegment) -> T

) {
    fun by(match: (matches: MatchSequence<T>) -> List<T>) = MatchSequence(data, regex, converter, match).result()
}

class MatchSequence<T : Any>(
    val data: TextSegment,
    private val regex: Regex,
    val converter: (data: TextSegment) -> T,
    val match: (matches: MatchSequence<T>) -> List<T>
) {
    private var pos = 0
    private var result = ArrayList<T>()
    private var lastMatch = findNext(regex)
    val range get() = lastMatch?.range ?: IntRange(pos, pos - 1)
    val segment get() = segment(range)
    val text get() = segment.toString()

    private fun progress() {
        lastMatch?.let {
            // pos still has value of before getting lastMatch
            nonMatch(it.range.first)
            jump(it)  // jump before the match conversion, so that jumps during conversion are effective
            result.addAll(match(this))
            lastMatch = findNext(regex)
        }

        if (lastMatch == null) {
            nonMatch(data.length)
        }
    }

    fun hasGroup(name: String) = lastMatch?.groups?.get(name) != null
    fun <R: Any> withGroup(name: String, action: (match: MatchGroup) -> R): R? {
        return lastMatch?.groups?.get(name)?.let { action(it) }
    }

    private fun nonMatch(to: Int) = result.addAll(noMatch(to))

    fun noMatch(to: Int): List<T> {
        if (pos < to) {
            val old = pos
            pos = to
            return listOf(converter(data.subSegment(old, to - 1)))
        }
        return listOf()
    }

    fun segment(range: IntRange): TextSegment = data.subSegment(range.first, range.last)

    /**
     * find on rthe unmatched part of the match sequence
     */
    fun findNext(regex: Regex): MatchResult? = regex.find(data, pos)

    /**
     * find from the start of the current match
     */
    fun refindNext(regex: Regex): MatchResult? = regex.find(data, range.first)

    fun jump(to: MatchResult) {
        pos = to.range.last + 1
    }

    fun result(): List<T> {
        do { progress() } while (lastMatch != null && pos < data.length)
        return result
    }

    fun rest(): TextSegment {
        val old = pos
        pos = data.length
        return data.subSegment(old, pos - 1)
    }

    override fun toString() = data.toString() + regex
}
