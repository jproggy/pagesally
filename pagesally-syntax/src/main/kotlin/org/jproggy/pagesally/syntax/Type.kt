package org.jproggy.pagesally.syntax

object Type {
    val DIGITS = "DIGIT"
    val FUNCTION = "FUNCTION"
    val LABEL = "LABEL"
    val MARKUP = "MARKUP"
    val OPERATOR = "OPERATOR"
    val INVALID = "INVALID"
    val DEFAULT = "DEFAULT"
    private val special = arrayOf(DIGITS, FUNCTION, LABEL, MARKUP, OPERATOR, INVALID, DEFAULT)

    val COMMENT_BLOCK = "COMMENT1"
    val COMMENT_LINE = "COMMENT2"
    val COMMENT_DOC = "COMMENT3"
    val COMMENT_4 = "COMMENT4"
    private val comment = arrayOf(COMMENT_BLOCK, COMMENT_LINE, COMMENT_DOC, COMMENT_4)

    val KEYWORD_1 = "KEYWORD1"
    val KEYWORD_2 = "KEYWORD2"
    val KEYWORD_3 = "KEYWORD3"
    val KEYWORD_4 = "KEYWORD4"
    private val keywords = arrayOf(KEYWORD_1, KEYWORD_2, KEYWORD_3, KEYWORD_4)

    val STRING = "LITERAL1"
    val LITERAL_WORD = "LITERAL2"
    val LITERAL_3 = "LITERAL3"
    val LITERAL_4 = "LITERAL4"
    private val literals = arrayOf(STRING, LITERAL_WORD, LITERAL_3, LITERAL_4)

    private val all = listOf(*special, *comment, *keywords, *literals)

    operator fun contains(value: String?) = all.contains(value)
    operator fun get(name: String?) : String {
        if (contains(name)) return name!!
        return DEFAULT
    }
    operator fun get(name: String?, defVal: String) : String {
        if (contains(name)) return name!!
        return defVal
    }
}