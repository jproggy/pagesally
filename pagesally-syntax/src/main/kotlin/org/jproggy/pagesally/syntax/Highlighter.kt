package org.jproggy.pagesally.syntax

import org.jproggy.snippetory.*
import org.jproggy.snippetory.spi.*
import org.jproggy.snippetory.util.CharDataSupport
import org.jproggy.snippetory.util.SimpleFormat
import org.jproggy.snippetory.util.TemplateNode

class Highlighter : FormatFactory, Configurer {
    override fun create(definition: String, ctx: TemplateContext): FormatConfiguration {
        return Language.byName(definition)?.let {
             Renderer(it)
        } ?: throw SnippetoryException("$definition not supported for highlighting")
    }

    private inner class Renderer(private val language: Language) : SimpleFormat(), VoidFormat {
        override fun format(location: TemplateNode?, value: Any?): Any {
            val highlighter = tpl["highlighter"].set("syntax", language.name)
            val text = TextSegment(CharDataSupport.toCharSequence(value))
            language.parse(text).forEach {
                token(it.type).set("value", it.text).render(highlighter, "target")
            }
            return highlighter
        }

        override fun formatVoid(node: TemplateNode) = format(node, node.region())

        override fun controlsRegion() = true

        override fun supports(value: Any?) = CharDataSupport.isCharData(value)
    }

    companion object {
        val tpl: Template

        fun token(type: String): Template {
            var token = tpl[type]
            if (!token.isPresent) token = tpl["token"]
            return token.set("type", type)
        }

        init {
            Format.register("highlight", Highlighter())
            tpl = Repo.readResource("org/jproggy/pagesally/syntax/highlighter.html")
                .syntax(Syntaxes.FLUYT_X)
                .encoding(Encodings.xml) // we assume pre white space handling so don't replace line breaks
                .parse()
        }
    }
}

