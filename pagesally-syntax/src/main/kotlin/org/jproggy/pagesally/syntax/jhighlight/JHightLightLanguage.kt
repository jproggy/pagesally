package org.jproggy.pagesally.syntax.jhighlight

import com.uwyn.jhighlight.highlighter.ExplicitStateHighlighter
import com.uwyn.jhighlight.highlighter.GroovyHighlighter
import com.uwyn.jhighlight.highlighter.JavaHighlighter
import com.uwyn.jhighlight.highlighter.XmlHighlighter
import org.jproggy.pagesally.syntax.Language
import org.jproggy.pagesally.syntax.TextSegment
import org.jproggy.pagesally.syntax.Type
import org.jproggy.pagesally.syntax.WordToken

class JHightLightLanguage private constructor(override val name: String, val converter: Map<Byte, String>,
        val parserFab: () -> ExplicitStateHighlighter) : Language {
    override fun parse(text: TextSegment): List<WordToken> {
        val parser = parserFab()
        parser.setReader(text.toString().reader())
        var index = 0
        var size = 0
        val result: MutableList<WordToken> = ArrayList()
        var oldType: String? = null
        while (index + size < text.length) {
            val type: String = converter[parser.nextToken] ?: Type.DEFAULT
            if (type != oldType && oldType != null) {
                result.addAll(WordToken.from(text.subSegment(index, (index + size) - 1), oldType))
                index += size
                size = 0
            }
            size += parser.tokenLength
            oldType = type
        }
        if (size > 0 && oldType != null) {
            result.add(WordToken(text.subSegment(index, (index + size) - 1), oldType))
        }
        return result
    }

    companion object {
        fun java(): JHightLightLanguage {
            JavaHighlighter.ASSERT_IS_KEYWORD = true
            return JHightLightLanguage("java", mapOf(
                0.toByte() to "invalid",
                JavaHighlighter.PLAIN_STYLE to Type.DEFAULT,
                JavaHighlighter.KEYWORD_STYLE to Type.KEYWORD_2,
                JavaHighlighter.TYPE_STYLE to Type.KEYWORD_1,
                JavaHighlighter.OPERATOR_STYLE to Type.OPERATOR,
                JavaHighlighter.SEPARATOR_STYLE to Type.OPERATOR,
                JavaHighlighter.LITERAL_STYLE to Type.STRING,
                JavaHighlighter.JAVA_COMMENT_STYLE to Type.COMMENT_LINE,
                JavaHighlighter.JAVADOC_COMMENT_STYLE to Type.COMMENT_DOC,
                JavaHighlighter.JAVADOC_TAG_STYLE to Type.LABEL
            )) { JavaHighlighter() }
        }
        fun groovy() = JHightLightLanguage("groovy", mapOf(
            GroovyHighlighter.PLAIN_STYLE to Type.DEFAULT,
            GroovyHighlighter.KEYWORD_STYLE to Type.KEYWORD_2,
            GroovyHighlighter.TYPE_STYLE to Type.KEYWORD_1,
            GroovyHighlighter.OPERATOR_STYLE to Type.OPERATOR,
            GroovyHighlighter.SEPARATOR_STYLE to Type.OPERATOR,
            GroovyHighlighter.LITERAL_STYLE to Type.STRING,
            GroovyHighlighter.JAVA_COMMENT_STYLE to Type.COMMENT_LINE,
            GroovyHighlighter.JAVADOC_COMMENT_STYLE to Type.COMMENT_DOC,
            GroovyHighlighter.JAVADOC_TAG_STYLE to Type.LABEL
        )) { GroovyHighlighter() }

        fun xml() = JHightLightLanguage("xml", mapOf(
            XmlHighlighter.PLAIN_STYLE to Type.DEFAULT,
            XmlHighlighter.ATTRIBUTE_NAME to Type.KEYWORD_1,
            XmlHighlighter.ATTRIBUTE_VALUE to Type.STRING,
            XmlHighlighter.CHAR_DATA to Type.LITERAL_3,
            XmlHighlighter.COMMENT to Type.COMMENT_BLOCK,
            XmlHighlighter.TAG_SYMBOLS to Type.OPERATOR,
            XmlHighlighter.TAG_NAME to Type.MARKUP,
            XmlHighlighter.PROCESSING_INSTRUCTION to Type.KEYWORD_3
        )) { XmlHighlighter() }

        fun register() {
            Language.register("java", java())
            Language.register("groovy", groovy())
            val xmlDef = xml()
            Language.register("xml", xmlDef)
            Language.register("html", xmlDef)
            Language.register("xhtml", xmlDef)
        }
    }
}