package org.jproggy.pagesally.syntax.regex

import org.jproggy.pagesally.syntax.Language
import java.util.*


private const val WORD_CHARS = "a-zA-Z0-9"

class RegexLanguage private constructor(override val name:String, type: String = "DEFAULT") : Syntax(type), Language {
    private val subSyntaxes: MutableMap<String, SubSyntax> = TreeMap(String.CASE_INSENSITIVE_ORDER)

    override var wordChars: String = WORD_CHARS + '_'
        get() {
            return field
        }
        set(value) {
            var v = value;
            arrayOf("\\", "-", "[", "]").forEach {
                v = v.replace(it, "\\" + it)
            }
            field = WORD_CHARS + v
        }

    override fun getSubSyntax(name: String): Syntax {
        if (name.equals("main", ignoreCase = true)) return this
        return subSyntaxes[name] ?: throw IllegalArgumentException(this.name + "::" + name + " not found.")
    }

    override fun subSyntax(name: String, defaultType: String, content: (end: String) -> String?): Syntax {
        subSyntaxes[name]?.let {
            return it
        }
        val s = SubSyntax(defaultType, content, name)
        subSyntaxes[name] = s
        return s
    }

    open inner class SubSyntax(override val defaultType: String, private val content: (end: String) -> String?,
                               override val name: String) : Syntax(defaultType) {
         override     var wordChars: String = ""
             get() {
                 return if (field.isEmpty()) { this@RegexLanguage.wordChars } else field
             }
             set(value) {
                 field = "&&[$value]"
             }
        override val allowedContent: (String) -> String?
            get() = content
        override fun subSyntax(name: String, defaultType: String, content: (end: String) -> String?) =
            this@RegexLanguage.subSyntax(name, defaultType, content)

        override fun getSubSyntax(name: String) = this@RegexLanguage.getSubSyntax(name)
        override fun toString() = this@RegexLanguage.name + "::" + name
    }

    override fun toString() = name
    companion object {
        fun define(name: String): RegexLanguage {
            val l = RegexLanguage(name)
            Language.register(name, l)
            return l
        }

        fun byName(name: String): RegexLanguage {
            val l = Language.byName(name)
            if (l is RegexLanguage) return l
            throw IllegalArgumentException("No Language called $name found.")
        }
    }

}
