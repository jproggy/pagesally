package org.jproggy.pagesally.syntax.importers

import org.jproggy.pagesally.syntax.Type
import org.jproggy.pagesally.syntax.regex.Content
import org.jproggy.pagesally.syntax.regex.Content.LINE
import org.jproggy.pagesally.syntax.regex.Content.LINE_END
import org.jproggy.pagesally.syntax.regex.RegexLanguage
import org.jproggy.pagesally.syntax.regex.Syntax
import org.jproggy.pagesally.syntax.regex.SyntaxReference
import org.jproggy.pagesally.xml.Xml
import org.jproggy.pagesally.xml.Xml.byName

class JEditImporter(name:String) {
    var language = RegexLanguage.define(name)

    fun readResource(name: String) {
        readXml(Xml.parseResource(name))
    }

    fun readXml(content: Xml.Document) {
        val rules = content.element.children.byName("RULES")
        rules.forEach { parseRuleSet(it) }
    }

    private fun parseRuleSet(ruleSet: Xml.Tag) {
        val name = ruleSet["SET"]
        val useDigits = ruleSet["HIGHLIGHT_DIGITS"] == "TRUE"
        val digitRe = ruleSet["DIGIT_RE"] ?: """\b[0-9]+\b"""
        val parser = if (name == null) {
            if (useDigits) language.regexToken(Type.DIGITS, digitRe.toRegex(language.flags()))
            RuleSetParser(language, ruleSet)
        } else {
            val subSyntax = language.subSyntax(name, ruleSet["DEFAULT"] ?: Type.DEFAULT,
                ruleSet.containedChars(language))
            if (useDigits) subSyntax.regexToken(Type.DIGITS, digitRe.toRegex(subSyntax.flags()))
            RuleSetParser(subSyntax, ruleSet)
        }
        ruleSet.children.forEach { parser.parseRule(it) }
    }


    inner class RuleSetParser(val syntax: Syntax, ruleSet: Xml.Tag) {
        fun parseRule(rule: Xml.Tag) {
            when(rule.name) {
                "SPAN" -> parseSpan(rule)
                "SPAN_REGEXP" -> parseSpanRegex(rule)
                "EOL_SPAN"-> parseLineSpan(rule)
                "EOL_SPAN_REGEXP"-> parseLineSpanRegex(rule)
                "SEQ" -> parseOp(rule)
                "SEQ_REGEXP" -> parseOpRegEx(rule)
                "MARK_FOLLOWING" -> parseMarkFollowing(rule)
                "MARK_PREVIOUS" -> parseMarkPrevious(rule)
                "KEYWORDS" -> parseKeywords(rule)
                "IMPORT" -> parseImport(rule)
                "TERMINATE" -> parseTerminate(rule)
                "PROPS" -> Unit
                else -> throw IllegalArgumentException("Rule ${rule.name} not supported")
            }
        }

        private fun parseSpan(rule: Xml.Tag) {
            val token = syntax.region(rule.marker(syntax), Regex.escape(rule.start), rule.containedChars(syntax)(rule.end),
                rule.endRegex(syntax), rule.type, rule.matchType)
            token.delegate = rule.syntaxRef
        }

        private fun parseSpanRegex(rule: Xml.Tag) {
            val token = syntax.breakOut(rule.marker(syntax) + rule.start, rule.containedChars(syntax),
                rule.endRegex(syntax), rule.type, rule.matchType)
            token.delegate = rule.syntaxRef
        }

        private fun parseLineSpan(rule: Xml.Tag) {
            val token = syntax.region(rule.marker(syntax), Regex.escape(rule.content), LINE + LINE_END, "", rule.type,
                rule.matchType)
            token.delegate = rule.syntaxRef
        }

        private fun parseLineSpanRegex(rule: Xml.Tag) {
            val token = syntax.region(rule.marker(syntax), rule.content, LINE + LINE_END, "", rule.type, rule.matchType)
            token.delegate = rule.syntaxRef
        }

        private fun parseOp(rule: Xml.Tag) {
            val marker = rule.marker(syntax)
            rule.syntaxRef?.let {
                syntax.regexToken(rule.type, (marker + Regex.escape(rule.content)).toRegex(syntax.flags()), it)
            } ?: if (marker == "") {
                syntax.keyNonWord(rule.type, rule.content)
            } else {
                syntax.regexToken(rule.type, (rule.marker(syntax) + Regex.escape(rule.content)).toRegex(syntax.flags()))
            }
        }

        private fun parseOpRegEx(rule: Xml.Tag) {
            rule.syntaxRef?.let {
                syntax.regexToken(rule.type, (rule.marker(syntax) + rule.content).toRegex(syntax.flags()), it)
            } ?: syntax.regexToken(rule.type, (rule.marker(syntax) + rule.content).toRegex(syntax.flags()))
        }

        private fun parseMarkFollowing(rule: Xml.Tag) {
            syntax.tokenStart(rule.marker(syntax), Regex.escape(rule.content), rule.matchType, rule.type)
        }

        private fun parseMarkPrevious(rule: Xml.Tag) {
            val expression = Regex.escape(rule.content)
            syntax.tokenEnd(rule.marker(syntax), expression, rule.matchType, rule.type)
        }

        private fun parseKeywords(rule: Xml.Tag) {
            rule.children.forEach {
                syntax.keyword(Type[it.name], it.content)
            }
        }

        private fun parseImport(rule: Xml.Tag) {
            syntax.include(rule.syntaxRef!!)
        }

        private fun parseTerminate(rule: Xml.Tag) {
            // is ignored only for plain text performance
        }

        init {
            syntax.caseSensitive = "FALSE".equals(ruleSet["IGNORE_CASE"], ignoreCase = true)
            ruleSet["NO_WORD_SEP"]?.let { syntax.wordChars = it }
        }
    }
    companion object {
        fun readCatalog(catalog: Xml.Document) {
            catalog.element.children.byName("MODE").forEach {
                JEditImporter(it["NAME"]!!).readResource("/" + it["FILE"]!!)
            }
        }
    }
}
const val AT_LINE_START = "AT_LINE_START"
const val AT_WHITESPACE_END = "AT_WHITESPACE_END"
const val AT_WORD_START = "AT_WORD_START"
const val DELEGATE = "DELEGATE"
const val ESCAPE = "ESCAPE"
const val NO_LINE_BREAK = "NO_LINE_BREAK"
const val NO_WORD_BREAK = "NO_WORD_BREAK"

val Xml.Tag.wordStart get() = this[AT_WORD_START] == "TRUE"
val Xml.Tag.lineStart get() = this[AT_LINE_START] == "TRUE"
val Xml.Tag.whitespaceEnd get() = this[AT_WHITESPACE_END] == "TRUE"
val Xml.Tag.singleLine get() = this[NO_LINE_BREAK] == "TRUE"
val Xml.Tag.singleWord get() = this[NO_WORD_BREAK] == "TRUE"

val Xml.Tag.type get() = Type[this["TYPE"]]
val Xml.Tag.matchType get() = Type[this["MATCH_TYPE"] ?: this["TYPE"]]


val Xml.Tag.start get() = this.child("BEGIN").content
val Xml.Tag.end get() = this.child("END").content
fun Xml.Tag.endRegex(s: Syntax): String {
    val end = this.child("END")
    if ((!name.endsWith("REGEXP") && end["REGEXP"] != "TRUE") || (name.endsWith("REGEXP") && end["REGEXP"] == "FALSE")) {
        return end.marker(s) + Regex.escape(end.content)
    }
    return end.marker(s) + end.content
}

fun Xml.Tag.containedChars(s: Syntax): (String) -> String? {
    if (singleWord) return {_ -> s.word}

    this[ESCAPE]?.let { esc ->
        require(esc.length == 1) { "Can just escape with single chars" }
        return { end: String ->
            //require(end.length == 1) { "Can just escape single end chars" }
            Content.escaped(esc[0], end[0], !singleLine)
        }
    }
    if (singleLine) return {_ -> LINE }
    return {_ -> null }
}

fun Xml.Tag.marker(s: Syntax) :String = when {
  this.lineStart -> "(?<=^)"
  this.whitespaceEnd -> """(?<=^\s*)"""
  this.wordStart -> s.wordStart
  else -> ""
}

val Xml.Tag.syntaxRef: SyntaxReference? get() {
    val name = this[DELEGATE] ?: return null
    if (name.contains("::")) {
        val parts = name.split("::")
        return SyntaxReference(parts[0], parts[1])
    }
    return SyntaxReference(syntax = name)
}
