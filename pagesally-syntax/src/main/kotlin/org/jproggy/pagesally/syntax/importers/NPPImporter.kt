package org.jproggy.pagesally.syntax.importers

import org.jproggy.pagesally.syntax.regex.Content
import org.jproggy.pagesally.syntax.regex.RegexLanguage
import org.jproggy.pagesally.syntax.regex.Region
import org.jproggy.pagesally.xml.Xml
import org.jproggy.pagesally.xml.Xml.Tag
import org.xml.sax.InputSource
import java.io.StringReader

class NPPImporter {
    fun readContent(content: String) {
        readContent(InputSource(StringReader(content)))
    }

    fun readContent(content: InputSource) {
        Xml.parse(content).element.children.forEach { l ->
            l.children.forEach { langTag: Tag -> parseLanguage(langTag) }
        }
    }

    private fun parseLanguage(langTag: Tag) {
        val language: RegexLanguage = langTag["name"]?.let { RegexLanguage.define(it)}!!
        langTag["commentLine"]?.let { pre -> language.regexToken("linecomment", Region.from(pre).build()) }
        val commentStart = langTag["commentStart"]
        val commentEnd = langTag["commentEnd"]
        if (commentStart != null && commentEnd != null) {
            language.regexToken("blockComment", Region.from(commentStart).to(commentEnd).build())
        }
        langTag.children.forEach { kw -> parseKeywords(language, kw) }
    }

    private fun parseKeywords(language: RegexLanguage, kwTag: Tag) {
        val type: String = kwTag["name"]!!
        val keywords: List<String> = kwTag.content.split(Content.WORD)
        for (kw in keywords) {
            language.keyword(type, kw)
        }
    }
}