package org.jproggy.pagesally.syntax.regex

class SyntaxReference(val language: String? = null, val syntax: String? = null) {
    fun resolve(context: Syntax) : Syntax {
        if (language == null) {
            syntax?.let {
                return context.getSubSyntax(it)
            } ?: throw IllegalArgumentException("Either language or syntax need to be provided")
        }
        val lang = RegexLanguage.byName(language)
        syntax?.let {
            return lang.getSubSyntax(it)
        } ?: return lang
    }

    override fun toString() = (language ?: "") + (syntax?.let { "::$it" } ?: "")
}