package org.jproggy.pagesally.syntax

import java.util.*

interface Language {
    val name: String

    fun parse(text: TextSegment): List<WordToken>

    companion object {
        private val DEFINITIONS: MutableMap<String, Language> = TreeMap(String.CASE_INSENSITIVE_ORDER)
        fun byName(name: String): Language? {
            return DEFINITIONS[name]
        }

        fun register(name: String, l: Language) {
            DEFINITIONS[name] = l
        }
    }
}

data class WordToken(val text: TextSegment, val type: String) {
    override fun toString(): String {
        return text.toString()
    }

    companion object {
        fun from(text: TextSegment, type: String): Array<WordToken> {
            if (text.length > 0) {
                return arrayOf(WordToken(text, type))
            }
            return arrayOf()
        }
    }
}