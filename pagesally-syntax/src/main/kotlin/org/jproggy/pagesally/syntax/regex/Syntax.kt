package org.jproggy.pagesally.syntax.regex

import org.jproggy.pagesally.syntax.MatchSequence
import org.jproggy.pagesally.syntax.TextSegment
import org.jproggy.pagesally.syntax.WordToken
import java.util.*

abstract class Syntax(open val defaultType: String) {
    abstract val name: String
    open val allowedContent: (end: String) -> String? get() = {_ -> null}
    private val keywords: MutableMap<String, String> = TreeMap(String.CASE_INSENSITIVE_ORDER)
    private val nonWords: MutableMap<String, String> = HashMap()
    private val elements: MutableList<SyntaxElement> = ArrayList()
    private val includes: MutableList<SyntaxReference> = ArrayList()

    var caseSensitive: Boolean = false
    abstract var wordChars: String

    fun include(ref: SyntaxReference) = includes.add(ref)

    protected open fun patterns(): List<String> {
        val pattern: MutableList<String> = ArrayList()
        if (elements.isNotEmpty()) {
            pattern.add(
                elements.map(SyntaxElement::pattern).joinToString("|")
            )
        }
        if (keywords.isNotEmpty()) {
            val p = keywords.keys.joinToString(prefix = """(?<!$word)(?:""", separator = "|", postfix = """)(?!$word)""") {
                Regex.escape(it)
            }
            pattern.add(p)
        }
        if (nonWords.isNotEmpty()) {
            val p = nonWords.keys.joinToString(prefix = "(?:", separator = "|", postfix = ")") {
                Regex.escape(it)
            }
            pattern.add(p)
        }
        includes.forEach {
            pattern.addAll(it.resolve(this).patterns())
        }
        return pattern
    }

    val word: String
        get() { return "[$wordChars]" }

    val wordStart: String
        get() { return """(?<=[^$wordChars]|^)""" }

    public fun flags(): Set<RegexOption> = if (caseSensitive) {
        setOf(RegexOption.MULTILINE)
    } else {
        setOf(RegexOption.MULTILINE, RegexOption.IGNORE_CASE)
    }

    fun parse(text: TextSegment): List<WordToken> {
        val p = patterns().joinToString("|")
        if (p.isEmpty()) {
            return listOf(WordToken(text, defaultType))
        }
        return text.transform(p.toRegex(flags())) {
            WordToken(it, defaultType)
        }.by {
            toTokens(it)
        }
    }

    open fun toTokens(matches: MatchSequence<WordToken>): List<WordToken> {
        return handleParts(matches) ?: listOf(WordToken(matches.segment, defaultType))
    }

    protected fun handleParts(matches: MatchSequence<WordToken>): List<WordToken>? {
        elements.firstOrNull {
            it.represents(matches)
        }?.let {
            return it.toTokens(matches)
        }

        keywords[matches.text]?.let { return listOf(WordToken(matches.segment, it)) }
        nonWords[matches.text]?.let { return listOf(WordToken(matches.segment, it)) }

        includes.forEach { includee ->
            includee.resolve(this).handleParts(matches)?.let { return it }
        }
        return null
    }

    abstract fun subSyntax(name: String, defaultType: String, content: (end: String) -> String?): Syntax
    abstract fun getSubSyntax(name: String): Syntax

    fun keyword(type: String, def: String) {
        keywords[def] = type
    }

    fun regexToken(type: String, def: Regex) {
        elements.add(RegexToken(type, def))
    }

    fun regexToken(type: String, def: Regex, ref: SyntaxReference) {
        elements.add(RestDelegate(type, def, ref))
    }

    fun keyNonWord(type: String, def: String) {
        nonWords[def] = type
    }

    fun tokenStart(marker: String, start: String, startType: String, tokenType: String) {
        elements.add(TokenStart(marker, start, startType, tokenType, word))
    }

    fun tokenEnd(marker: String, end: String, endType: String, tokenType: String) {
        elements.add(TokenEnd(marker, end, endType, tokenType, word))
    }

    fun breakOut(start: String, content: (String) -> String?, end: String, tokenType: String, matchType: String): BreakoutToken {
        val token = BreakoutToken(start.toRegex(flags()), content, end, tokenType, matchType, nextId())
        elements.add(token)
        return token
    }

    private fun nextId(): String {
        return  "x" + id++
    }

    fun region(
        marker: String = "",
        start: String,
        content: String? = null,
        end: String = "",
        tokenType: String,
        matchType: String? = null
    ): RegionToken {
        val token = RegionToken(marker, start, content, end, tokenType, matchType ?: tokenType)
        elements.add(token)
        return token
    }

    companion object {
        private var id: Int = 1
    }

    inner class RestDelegate(val type: String, override val definition: Regex, private val ref: SyntaxReference) :
        SyntaxElement {
        override fun toTokens(matches: MatchSequence<WordToken>) = listOf(
            *WordToken.from(matches.segment, type),
            *ref.resolve(this@Syntax).parse(matches.rest()).toTypedArray()
        )
    }

    inner class RegionToken(
        val marker: String,
        val start: String,
        val content: String?,
        val end: String,
        override val type: String,
        val matchType: String
    ) : DelegatingToken(type) {
        private val allowedContent: String get() = content ?: this@Syntax.allowedContent(end) ?:Content.ALL;
        override val definition: Regex
            get() = (marker + start + allowedContent + end).toRegex(this@Syntax.flags())

        override fun toTokens(matches: MatchSequence<WordToken>): List<WordToken> {
            if (type == matchType && delegate == null) return listOf(*WordToken.from(matches.segment, type))
            val regex = "$start($allowedContent)$end".toRegex(this@Syntax.flags())
            regex.find(matches.segment)?.let {
                val matchGroup = it.groups[1] ?: throw IllegalStateException("${matches.segment} unexpected")
                return listOf(
                    *WordToken.from(matches.segment.subSegment(endIndex = matchGroup.range.first - 1), matchType),
                    *content(matches.segment.subSegment(matchGroup.range)),
                    *WordToken.from(matches.segment.subSegment(startIndex = matchGroup.range.last + 1), matchType)
                )
            } ?: throw IllegalStateException("${matches.segment} unexpected")
        }
    }

    inner class BreakoutToken(
        override val definition: Regex,
        val chars: (end: String) -> String?,
        val end: String,
        override val type: String,
        val matchType: String,
        val id: String
    ) : DelegatingToken(type) {
        fun allowedContent(end: String): String = this.chars(end) ?: this@Syntax.allowedContent(end) ?: Content.ALL;

        override val pattern: String
            get() = "(?<$id>$definition)"

        override fun toTokens(matches: MatchSequence<WordToken>): List<WordToken> {
            val first = matches.range.first
            val found = matches.refindNext(definition)!!
            matches.findNext(endRegex(found))!!.let {
                matches.jump(it)
                val matchGroup = it.groups[1] ?: throw IllegalStateException("${matches.segment} unexpected")
                return if ((type == matchType && delegate == null) || matchGroup.range.isEmpty()) {
                    listOf(*WordToken.from(matches.data.subSegment(first, it.range.last), matchType))
                } else {
                    listOf(
                        *WordToken.from(matches.segment(matches.range), matchType),
                        *content(matches.segment(matchGroup.range)),
                        *WordToken.from(matches.data.subSegment(matchGroup.range.last+1, it.range.last), matchType)
                    )
                }
            }
        }

        override fun represents(matches: MatchSequence<WordToken>): Boolean {
            if (!matches.hasGroup(id)) return false
            val found = matches.refindNext(definition) ?: return false
            return matches.findNext(endRegex(found)) != null
        }

        private fun endRegex(found: MatchResult): Regex {
            var x = end
            for (i in 1 until found.groups.size) {
                found.groups[i]?.let {
                    x = x.replace("$$i", it.value)
                }
            }

            return ("(${allowedContent(x)})$x").toRegex(this@Syntax.flags())
        }
    }

    abstract inner class DelegatingToken(open val type: String) : SyntaxElement {
        var delegate: SyntaxReference? = null

        protected fun content(content: TextSegment): Array<WordToken> {
            return delegate?.resolve(this@Syntax)?.parse(content)?.toTypedArray() ?: WordToken.from(content, type)
        }
    }
}

data class RegexToken(val type: String, override val definition: Regex) : SyntaxElement {
    override fun toTokens(matches: MatchSequence<WordToken>) = listOf(WordToken(matches.segment, type))
}

data class TokenStart(val marker: String, val start: String, val startType: String, val tokenType: String, val word: String) :
    SyntaxElement {
    override val definition = "$marker($start)(${word}+)".toRegex(RegexOption.MULTILINE)
    override fun toTokens(matches: MatchSequence<WordToken>): List<WordToken> {
        val text = matches.segment
        if (startType == tokenType) return listOf(*WordToken.from(text, tokenType))
        val (start, token) = text.split(start.toRegex(RegexOption.MULTILINE))
        return listOf(*WordToken.from(start, startType), *WordToken.from(token, tokenType))
    }
}

data class TokenEnd(val marker: String, val end: String, val endType: String, val tokenType: String, val word: String) : SyntaxElement {
    override val definition = "$marker($word+)($end)".toRegex(RegexOption.MULTILINE)
    override fun toTokens(matches: MatchSequence<WordToken>): List<WordToken> {
        if (endType == tokenType) return listOf(WordToken(matches.segment, tokenType))
        val (token, end) = matches.segment.split(end.toRegex())
        return listOf(*WordToken.from(token, tokenType), *WordToken.from(end, endType))
    }
}

interface SyntaxElement {
    val definition: Regex
    val pattern: String get() = "(?:$definition)"

    fun toTokens(matches: MatchSequence<WordToken>): List<WordToken>
    fun represents(matches: MatchSequence<WordToken>): Boolean = definition.matches(matches.segment)
}
