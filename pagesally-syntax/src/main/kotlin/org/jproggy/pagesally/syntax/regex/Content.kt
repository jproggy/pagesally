package org.jproggy.pagesally.syntax.regex


object Content {
    private const val LINE_END_CHARS = "\\n\\r\\u0085\\u2028\\u2029"
    private const val BACK_SLASH = '\\'
    private const val BACK_SLASH_QUOTED = "" + BACK_SLASH + BACK_SLASH

    const val LINE = "[^$LINE_END_CHARS]*"
    const val WORD = "\\w*+"
    const val ALL = "[\\s\\S]*?"
    const val LINE_END = "(?>(?>\\r\\n?)|\\n|\\u0085|\\u2028|\\u2029|\\Z)"
    fun escaped(escapeChar: Char, endChar: Char, multiLine: Boolean = false): String {
        val e = esc(escapeChar)
        val s = esc(endChar)
        return if (multiLine) {
            "(?:$e$e|$e$s|[^$s])*+"
        } else {
            "(?:$e$e|$e$s|[^$s$LINE_END_CHARS])*+"
        }
    }

    private fun esc(c: Char): String = when (c) {
        BACK_SLASH -> BACK_SLASH_QUOTED
        ')' -> "" + BACK_SLASH + c
        else -> "" + c
    }
}