package org.jproggy.pagesally.syntax.importers

import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.nulls.beNull
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNot
import org.jproggy.pagesally.syntax.Language
import org.jproggy.pagesally.syntax.TextSegment
import org.jproggy.pagesally.syntax.Type.COMMENT_BLOCK
import org.jproggy.pagesally.syntax.Type.COMMENT_LINE
import org.jproggy.pagesally.syntax.Type.DEFAULT
import org.jproggy.pagesally.syntax.Type.DIGITS
import org.jproggy.pagesally.syntax.Type.FUNCTION
import org.jproggy.pagesally.syntax.Type.KEYWORD_1
import org.jproggy.pagesally.syntax.Type.KEYWORD_2
import org.jproggy.pagesally.syntax.Type.LABEL
import org.jproggy.pagesally.syntax.Type.LITERAL_3
import org.jproggy.pagesally.syntax.Type.LITERAL_WORD
import org.jproggy.pagesally.syntax.Type.MARKUP
import org.jproggy.pagesally.syntax.Type.OPERATOR
import org.jproggy.pagesally.syntax.Type.STRING
import org.jproggy.pagesally.xml.Xml

val prog = """
    :xx
     :yy // no label
    xx: /test/
      \bla
    @anno(x)
    private String doIt(String what) {
      return "#what @xx\"" + 34;
    }
    #xx##xx#
    #A23#
      some text here
    #A23#
    tpl.set("x", Encodings.HTML.wrap("<b>markup</b>"));
    /*
      block
    */
""".trimIndent()

class JEditImporterTest : FunSpec({

    test("readContent") {
        val sut = JEditImporter("e")
        sut.readResource("/test-syntax.xml")
        val token = Language.byName("e")!!.parse(TextSegment(prog))
        token.joinToString("") shouldBe prog
        token.map { it.text.toString() to it.type } shouldBe listOf(
            ":xx\n" to LABEL, " :yy " to DEFAULT, "// no label\n" to COMMENT_LINE,
            "xx:" to LABEL, " " to DEFAULT, "/test/" to MARKUP, "\n  " to DEFAULT,
            "\\" to OPERATOR, "bla" to LITERAL_3, "\n" to DEFAULT,
            "@anno" to LITERAL_3, "(" to OPERATOR, "x" to DEFAULT, ")" to OPERATOR, "\n" to DEFAULT,
            "private" to KEYWORD_1, " " to DEFAULT, "String" to KEYWORD_2, " " to DEFAULT, "doIt" to FUNCTION,
            "(" to OPERATOR, "String" to KEYWORD_2, " what" to DEFAULT, ")" to OPERATOR, " {\n  return " to DEFAULT,
            "\"" to STRING, "#what" to LITERAL_WORD, " " to STRING, "@xx" to MARKUP, "\\\"" to STRING,
            "\"" to STRING, " " to DEFAULT, "+" to OPERATOR, " " to DEFAULT, "34" to DIGITS,
            ";\n}\n" to DEFAULT, "#xx##xx#" to OPERATOR,
            "\n" to DEFAULT, "#A23#" to OPERATOR, "\n  some text here\n" to LITERAL_WORD, "#A23#" to OPERATOR,
            "\ntpl" to DEFAULT, "." to OPERATOR, "set" to FUNCTION, "(" to OPERATOR, "\"" to STRING, "x" to STRING,
            "\"" to STRING, "," to OPERATOR, " Encodings" to DEFAULT, "." to OPERATOR, "HTML" to DEFAULT,
            "." to OPERATOR, "wrap" to FUNCTION, "(" to OPERATOR, "\"" to STRING, "<b>markup</b>" to STRING,
            "\"" to STRING, ")" to OPERATOR, ")" to OPERATOR,
            ";\n" to DEFAULT, "/*\n  block\n*/" to COMMENT_BLOCK
        )
    }

    test("readCatalog") {
        JEditImporter.readCatalog(Xml.parseResource("/catalog"))
        Language.byName("test-syntax") shouldNot beNull()
    }

    test("pattern") {
        val found = """(?<=\W|^)/[^\p{Blank}]*?/""".toRegex().find("/test/ ")!!.value
        found shouldBe "/test/"
    }
})
