package org.jproggy.pagesally.syntax

import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import org.jproggy.pagesally.syntax.jhighlight.JHightLightLanguage
import org.jproggy.snippetory.TemplateContext

val java = """
    String x = "test";
    /*
      comment
    */""".trimIndent()

class HighlighterTest : FunSpec({
    Language.register("java", JHightLightLanguage.java())
    val format = Highlighter().create("java", TemplateContext()).getFormat(null)
    test("java") {
        var result  = format.format(null, java).toString()
        result shouldBe  """
            <code class="java"><span class="KEYWORD1">String</span> x <span class="OPERATOR">=</span> <span class="LITERAL1">"test"</span><span class="OPERATOR">;</span>
            <span class="COMMENT2">/*
              comment
            */</span></code>
            
""".trimIndent()
    }
})

