package org.jproggy.pagesally.syntax.jhighlight

import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldStartWith
import org.jproggy.pagesally.syntax.TextSegment
import org.jproggy.pagesally.syntax.Type
import org.jproggy.pagesally.syntax.WordToken

val java = TextSegment("""
    String x = "test";
    /*
      comment
    */""".trimIndent())

val xml = TextSegment("""
    <test>
      <t:test val="xx" />
    </test>
    <!--  multiline comment
    ends in other line -->
    """.trimIndent())

class JHightLightLanguageTest : FunSpec({
    test("java") {
        val token = JHightLightLanguage.java().parse(java)
        token.map { it.type }.toString() shouldBe listOf(
            Type.KEYWORD_1, Type.DEFAULT, Type.OPERATOR, Type.DEFAULT, Type.STRING, Type.OPERATOR, Type.DEFAULT,
            Type.COMMENT_LINE
        ).toString()
        token.joinToString("") shouldBe java.toString()
        token.last().text.toString() shouldStartWith "/*"
    }

    test("xml") {
        val token = JHightLightLanguage.xml().parse(xml)
        token.map { it.type }.toString() shouldBe listOf(
            Type.OPERATOR, Type.MARKUP, Type.OPERATOR, Type.DEFAULT, Type.OPERATOR, Type.MARKUP, Type.DEFAULT,
            Type.KEYWORD_1, Type.OPERATOR, Type.STRING, Type.DEFAULT, Type.OPERATOR, Type.DEFAULT, Type.OPERATOR,
            Type.MARKUP, Type.OPERATOR, Type.DEFAULT, Type.COMMENT_BLOCK, Type.DEFAULT, Type.COMMENT_BLOCK
        ).toString()
        token.joinToString("") shouldBe xml.toString()
        token[5].text.toString() shouldBe "t:test"
    }
})
