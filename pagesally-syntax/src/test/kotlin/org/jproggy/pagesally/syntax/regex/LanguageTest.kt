package org.jproggy.pagesally.syntax.regex

import io.kotest.core.spec.style.StringSpec
import io.kotest.data.forAll
import io.kotest.data.row
import io.kotest.matchers.shouldBe
import org.jproggy.pagesally.syntax.Language
import org.jproggy.pagesally.syntax.TextSegment
import org.jproggy.pagesally.syntax.Type.COMMENT_BLOCK
import org.jproggy.pagesally.syntax.Type.COMMENT_LINE
import org.jproggy.pagesally.syntax.Type.DEFAULT
import org.jproggy.pagesally.syntax.Type.KEYWORD_1
import org.jproggy.pagesally.syntax.Type.LABEL
import org.jproggy.pagesally.syntax.Type.STRING
import org.jproggy.pagesally.syntax.WordToken
import kotlin.math.min

class LanguageTest: StringSpec( {
    defineLang()

   "check parsing" {
        forAll(
            row("hallo", listOf(DEFAULT)) ,
            row("publichallo", listOf(DEFAULT)),
            row("public hallo", listOf(KEYWORD_1, DEFAULT)),
            row("Public hallo", listOf(KEYWORD_1, DEFAULT)),
            row("public\$hallo", listOf(KEYWORD_1, LABEL)),
            row("\$hallo", listOf( LABEL)),
            row("\$hallo// test", listOf(LABEL, COMMENT_LINE)),
            row("\$hallo/* test */", listOf(LABEL, COMMENT_BLOCK)),
            row("hallo/* \ntest\n */", listOf(DEFAULT, COMMENT_BLOCK)),
            row("hallo// test\u0085xx", listOf(DEFAULT, COMMENT_LINE, DEFAULT)),
            row("\"hallo\"", listOf(STRING)),
            row("\"Ha\\nl\\\\l\\\"fo\"", listOf(STRING)),
            row("\"\$Ha\n\\\"fo\"", listOf(DEFAULT, LABEL, DEFAULT, STRING)),
            row("\$xx\"Hallo\"", listOf(LABEL, STRING)),
            row("\"Hallo\\\"xx\"", listOf(STRING)),
            row("\"Hallo//in string\"", listOf(STRING)),
            row("\"a String\" + \"another String\"", listOf(STRING, DEFAULT, STRING)),
            row("""hallo// test
xx/* hallo 
this
are a 
few line*/more "stuff"""", listOf(
                DEFAULT, COMMENT_LINE, DEFAULT, COMMENT_BLOCK, DEFAULT, STRING
            ))
        ) { text, types ->

            val tokens: List<WordToken> = token(text)
            tokens.joinToString("") shouldBe text
            tokens.map { it.type }.toString() shouldBe types.toString()
        }
   }
})

fun token(text: String) = Language.byName("test1")!!.parse(TextSegment(text))

fun defineLang(): RegexLanguage {

    val test1 = RegexLanguage.define("test1")
    test1.region(
        start = Regex.escape("//"),
        content = Content.LINE + Content.LINE_END,
        tokenType = COMMENT_LINE
    )
    test1.region(
        start = Regex.escape("\""),
        content = Content.escaped('\\', '"'),
        end = Regex.escape("\""),
        tokenType = STRING
    )
    test1.region(
        start = Regex.escape("/*"),
        content = Content.ALL,
        end = Regex.escape("*/"),
        tokenType = COMMENT_BLOCK
    )
    test1.keyword(KEYWORD_1, "public")
    test1.regexToken(LABEL, "\\\$[a-zA-Z0-9]+".toRegex())
    return test1
}


