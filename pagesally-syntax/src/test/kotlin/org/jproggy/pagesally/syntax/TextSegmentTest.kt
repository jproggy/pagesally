package org.jproggy.pagesally.syntax

import io.kotest.assertions.asClue
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.beEmpty
import io.kotest.matchers.types.beInstanceOf

const val numbers = "01234567890123456789"
class TextSegmentTest: FunSpec( {
    test("empty") {
        val sut = TextSegment("")
        sut.length shouldBe 0
        sut.toString() should beEmpty()
        sut.subSegment(0, -1) shouldBe sut
        sut.subSequence(0, 0).asClue {
            it should beInstanceOf<TextSegment>()
            it shouldBe sut
        }
        shouldThrow<AssertionError> { sut.subSegment(0, 0) }
        shouldThrow<AssertionError> { sut.subSegment(-1, -1) }
        shouldThrow<AssertionError> { sut.subSegment(0, 1) }
        sut.transform(".".toRegex()) { it }.by { listOf(it.segment) }.size shouldBe 0
    }

    test("nums") {
        val sut = TextSegment(numbers);
        val sutCut = TextSegment(numbers, 5 .. 8)
        sut.length shouldBe 20
        sut.subSegment(5, 8).toString() shouldBe "5678"
        sutCut.toString() shouldBe "5678"
        sut.subSegment(5, 8) shouldBe sutCut
        sutCut.subSegment(2).toString() shouldBe "78"
        sutCut.subSegment(2).pos.first shouldBe 7
        sutCut.subSegment(2, 2).toString() shouldBe "7"
        sutCut.subSegment(2, 1).toString() should beEmpty()
        sutCut.transform("[57]".toRegex()) { it }.by { listOf(it.segment) }.size shouldBe 4
        sutCut.transform("[a]".toRegex()) { it }.by { listOf(it.segment) }.firstOrNull() shouldBe sutCut
    }

})