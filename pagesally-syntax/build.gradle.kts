val versions: Map<String, String> by rootProject.extra

dependencies {
    implementation("org.jproggy", "snippetory-core", versions["snippetory"])
    implementation ("com.uwyn", "jhighlight", "1.0") {
        exclude( group = "javax.servlet", module = "servlet-api")
    }
}

publishing.publications.named("pagesally", MavenPublication::class) {
    pom{
        description.set("Multiplatform syntax highlighting in kotlin")
        url.set("https://www.jproggy.org/pagesally/syntax/")
        licenses {
            license {
                name.set("The Apache License, Version 2.0")
                url.set("http://www.apache.org/licenses/LICENSE-2.0")
                distribution.set("repo")
            }
        }
    }
}
