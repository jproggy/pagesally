
rootProject.name = "pagesally"

include("pagesally-generator")
include("pagesally-syntax")
include("pagesally-syntax-rules")

pluginManagement {
    repositories {
        gradlePluginPortal()
    }
}
