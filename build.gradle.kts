import java.net.URI

plugins {
    kotlin("jvm") version "2.1.0"
    `maven-publish`
    signing
    java
    id("org.jetbrains.dokka") version "1.9.20"
    id("org.sonarqube") version "6.0.1.5171"
    jacoco
}

extra.apply {
    set("versions", mapOf(
        "java" to "21",
        "snippetory" to "1.0.0-SNAPSHOT",
        "log4j" to "2.14.0",
        "kotlin-logging" to "1.12.0",
        "slf4j" to "1.7.30",
        "kotest" to "4.3.2",
        "web-api" to "6.0.0",
        "coroutines" to "1.3.9"
    ))
}

repositories {
    mavenCentral()
    mavenLocal()
}

dependencies {
    implementation(kotlin("gradle-plugin"))
}

sonar {
    properties {
        property("sonar.projectKey", "jproggy_pagesally")
        property("sonar.organization", "jproggy")
        property("sonar.host.url", "https://sonarcloud.io")
        property("sonar.java.coveragePlugin", "jacoco")
        property("sonar.coverage.jacoco.xmlReportPaths", "build/reports/test/jacocoTestReport.xml")
        property("sonar.junit.reportPaths", "build/test-results/test")
    }
}


subprojects {
    val versions: Map<String, String> by rootProject.extra
    fun isReleaseVersion() = !version.toString().endsWith("-SNAPSHOT")
    fun isPublishing() = gradle.taskGraph.hasTask("publish")

    apply {
        plugin("kotlin")
        plugin("maven-publish")
        plugin("signing")
        plugin("org.jetbrains.dokka")
        plugin("jacoco")
    }

    repositories {
        mavenLocal()
        mavenCentral()
        maven {
            url = URI("https://oss.sonatype.org/content/repositories/snapshots")
        }
    }

    kotlin {
        jvmToolchain {
            (this as JavaToolchainSpec).languageVersion.set(JavaLanguageVersion.of(versions["java"]!!))
        }
    }

    group = "org.jproggy.pagesally"
    version = "1.0.0-SNAPSHOT"

    tasks.jacocoTestReport {
        reports {
            xml.required = true
            csv.required = false
        }
    }

    tasks.withType<Test> {
        useJUnitPlatform()
    }

    dependencies {
        implementation("org.jetbrains.kotlinx", "kotlinx-coroutines-core-jvm", versions["coroutines"])

        testImplementation(kotlin("reflect"))
        testImplementation(kotlin("test-junit"))
        testImplementation("io.kotest", "kotest-assertions-core", versions["kotest"])
        testImplementation("io.kotest", "kotest-framework-api-jvm", versions["kotest"])
        testRuntimeOnly("io.kotest", "kotest-runner-junit5", versions["kotest"])
    }

    java {
        withSourcesJar()
    }

    publishing {
        if (project.hasProperty("ossrhPassword")) {
            repositories {
                maven {
                    name = "oss"
                    val releaseRepo = "https://oss.sonatype.org/service/local/staging/deploy/maven2/"
                    val snapshotRepo = "https://oss.sonatype.org/content/repositories/snapshots/"
                    url = URI(if (isReleaseVersion()) releaseRepo else snapshotRepo)
                    credentials {
                        username = project.properties["ossrhUsername"] as String
                        password = project.properties["ossrhPassword"] as String
                    }
                }
            }
        }
    }

    val dokkaJavadocJar by tasks.register<Jar>("dokkaJavadocJar") {
        dependsOn(tasks.dokkaJavadoc)
        from(tasks.dokkaJavadoc.flatMap { it.outputDirectory })
        archiveClassifier.set("javadoc")
    }

    val pagesally by publishing.publications.creating(MavenPublication::class) {
        from( components["java"] )
        artifact(dokkaJavadocJar)
        pom {
            url.set("https://www.jproggy.org/pagesally/")
            developers {
                developer {
                    id.set("Sir RotN")
                    name.set("Bernd Ebertz")
                    email.set("bebertz@jproggy.org")
                }
            }
        }
    }

    signing {
        if( isReleaseVersion() && isPublishing() ) {
            sign(pagesally)
        }
    }
}