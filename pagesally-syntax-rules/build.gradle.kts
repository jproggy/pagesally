val versions: Map<String, String> by rootProject.extra

dependencies {
    implementation( project(":pagesally-syntax") )
    implementation("org.jproggy", "snippetory-core", versions["snippetory"])
}

publishing.publications.named("pagesally", MavenPublication::class) {
    pom.description.set("Edit modes for the pagesally syntax highlighter.".trimMargin())
    pom.licenses {
        license {
            name.set("GNU Public License, Version 2.0")
            url.set("https://www.gnu.de/documents/gpl-2.0.de.html")
            distribution.set("repo")
        }
    }
}
