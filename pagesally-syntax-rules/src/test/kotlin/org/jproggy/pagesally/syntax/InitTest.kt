package org.jproggy.pagesally.syntax

import io.kotest.core.spec.style.FunSpec
import io.kotest.data.forAll
import io.kotest.data.row
import io.kotest.matchers.shouldBe
import org.jproggy.pagesally.syntax.Type.DEFAULT
import org.jproggy.pagesally.syntax.Type.DIGITS
import org.jproggy.pagesally.syntax.Type.KEYWORD_1
import org.jproggy.pagesally.syntax.Type.KEYWORD_2
import org.jproggy.pagesally.syntax.Type.KEYWORD_3
import org.jproggy.pagesally.syntax.Type.LITERAL_4
import org.jproggy.pagesally.syntax.Type.LITERAL_WORD
import org.jproggy.pagesally.syntax.Type.MARKUP
import org.jproggy.pagesally.syntax.Type.OPERATOR
import org.jproggy.pagesally.syntax.Type.STRING


class InitTest : FunSpec({
  Init.loadAll()

  // different parts not compatible with
  test("batch") {
    val lang = Language.byName("batch")!!
    forAll(
      row("@echo off", listOf(OPERATOR, KEYWORD_1, LITERAL_4, LITERAL_4)),
      row("@echo off ", listOf(OPERATOR, KEYWORD_1, LITERAL_4, LITERAL_4, KEYWORD_3)),
      row("for %%a in (%list%) do", listOf(KEYWORD_1, DEFAULT, KEYWORD_2, DEFAULT, KEYWORD_3, DEFAULT, OPERATOR, KEYWORD_2, OPERATOR, DEFAULT, KEYWORD_3))
    ) { text: String, types: List<String> ->
      verify(lang, text, types)
    }
  }

  test("css").config(enabled = true) {
    val lang = Language.byName("css")!!
    forAll(
      row(""".font font{ font-size: 10pt; font-family: "Courier New"; }""", listOf(STRING, DEFAULT, KEYWORD_1, OPERATOR,
        DEFAULT, KEYWORD_2, OPERATOR, DEFAULT, DIGITS, OPERATOR, DEFAULT, KEYWORD_2, OPERATOR, DEFAULT, STRING, OPERATOR,
        DEFAULT, OPERATOR))
    ) { text: String, types: List<String> ->
      verify(lang, text, types)
    }
  }

  test("html") {
    val lang = Language.byName("html")!!
    forAll(
      row("""<i>&copy; JProggy.org</i>
<script>
  let a = { b: 15}
</script>
""", listOf(MARKUP, KEYWORD_1, MARKUP, LITERAL_WORD, DEFAULT, MARKUP, MARKUP, KEYWORD_1, MARKUP, DEFAULT, MARKUP,
        KEYWORD_1, MARKUP, DEFAULT, KEYWORD_1, DEFAULT, OPERATOR, DEFAULT, OPERATOR, DEFAULT,
        OPERATOR, DEFAULT, DIGITS, OPERATOR, DEFAULT, MARKUP, MARKUP, KEYWORD_1, MARKUP, DEFAULT))
    ) { text: String, types: List<String> ->
      verify(lang, text, types)
    }
  }

  test("properties") {
    val lang = Language.byName("properties")!!
    forAll(
      row("""
        entry1=v
        """.trimIndent(), listOf(KEYWORD_1, OPERATOR, STRING))
      ) { text: String, types: List<String> ->
        verify(lang, text, types)
      }
  }


  test("javascript") {

  val lang = Language.byName("javascript")!!
    forAll(
      row("var a={b:'test'}", listOf(KEYWORD_1, DEFAULT, OPERATOR, OPERATOR, DEFAULT, OPERATOR, STRING, OPERATOR)),
      row("let list=[1, 234]", listOf(KEYWORD_1, DEFAULT, OPERATOR, OPERATOR, DIGITS, OPERATOR, DEFAULT, DIGITS, OPERATOR))
    ) { text: String, types: List<String> ->

      verify(lang, text, types)
    }
  }
})

private fun verify(
  lang: Language,
  text: String,
  types: List<String>
) {
  var tokens: List<WordToken> = lang.parse(TextSegment(text))
  tokens.joinToString("") shouldBe text
  tokens.map { it.type }.toString() shouldBe types.toString()
}
